package com.example.mvmshop2;

import android.app.Activity;
import android.app.Instrumentation;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.platform.app.InstrumentationRegistry;

import androidx.test.espresso.Espresso;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.*;

@RunWith(AndroidJUnit4.class)
public class LoginActivityIntentTest
{
    private String phone = "1337";
    private String password = "mario";
    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);
    private LoginActivity mActivity = null;
    Instrumentation.ActivityMonitor monitor = InstrumentationRegistry.getInstrumentation().addMonitor(HomeActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception
    {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void testLaunchHomeActivityOnButtonClick()
    {
        assertNotNull(mActivity.findViewById(R.id.login_button));
        Espresso.onView(withId(R.id.login_phone_number_input)).perform(typeText(phone), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_password_input)).perform(typeText(password), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_button)).perform(click());
        Activity homeActivity = InstrumentationRegistry.getInstrumentation().waitForMonitorWithTimeout(monitor, 5000);
        assertNotNull(homeActivity);
    }

    @After
    public void tearDown() throws Exception
    {
        mActivity = null;
    }
}