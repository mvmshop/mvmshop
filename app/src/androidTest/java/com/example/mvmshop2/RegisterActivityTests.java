package com.example.mvmshop2;

import android.content.Intent;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import com.example.mvmshop2.ModelClasses.Users;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import org.junit.After;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static com.squareup.javawriter.JavaWriter.type;
import static org.junit.Assert.*;


@RunWith(AndroidJUnit4.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class RegisterActivityTests
{
    @Rule
    public ActivityTestRule<RegisterActivity> mActivityTestRule = new ActivityTestRule<RegisterActivity>(RegisterActivity.class);

    private String fullName = "arunas Balionas";
    private String emailAddress = "labas@gmail.com";
    private String phone = "1338123574";
    private String phone1 = "1337";
    public String mssage = "alio";
    private String password = "slaptazodisyraslaptazodis";
    private String password1 = "slaptazodisyraslaptazois2";

    @Before
    public void setUp() throws Exception
    {

    }

    @Test
    public void fullNameIsEmpty ()
    {

        Espresso.onView(withId(R.id.register_email_address_input)).perform(typeText(emailAddress),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_phone_number_input)).perform(typeText(phone),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.confirm_password_input)).perform(typeText(password1),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_button)).perform(click());
        Espresso.onView(withText("Įrašykite savo vardą")).inRoot(new ToastMatcher()).check(matches(isDisplayed()));

    }

    @Test
    public void confirmPassword ()
    {
        Espresso.onView(withId(R.id.register_full_name_input)).perform(typeText(fullName), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_email_address_input)).perform(typeText(emailAddress),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_phone_number_input)).perform(typeText(phone),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.confirm_password_input)).perform(typeText(password1),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_button)).perform(click());

        Espresso.onView(withText(R.string.toast_confirm_password)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

    @Test
    public void doesPhoneNumberExists ()
    {
        Espresso.onView(withId(R.id.register_full_name_input)).perform(typeText(fullName), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_email_address_input)).perform(typeText(emailAddress),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_phone_number_input)).perform(typeText(phone1),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.confirm_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_button)).perform(click());

        Espresso.onView(withText(R.string.toast_test_dublicate)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));

    }



    @Test
    public void testUserInputScenario()
    {
        Espresso.onView(withId(R.id.register_full_name_input)).perform(typeText(fullName), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_email_address_input)).perform(typeText(emailAddress),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_phone_number_input)).perform(typeText(phone),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.register_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.confirm_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());

        Espresso.onView(withId(R.id.register_button)).perform(click());

        final DatabaseReference RootRef;
        RootRef = FirebaseDatabase.getInstance().getReference();

        RootRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {

                Users usersData = dataSnapshot.child("Users").child(phone).getValue(Users.class);
                if(usersData != null)
                {

                    Espresso.onView(withId(R.id.register_full_name_input)).check(matches(withText(usersData.getFullName())));
                    Espresso.onView(withId(R.id.register_email_address_input)).check(matches(withText(usersData.getEmail())));
                    Espresso.onView(withId(R.id.register_phone_number_input)).check(matches(withText(usersData.getPhone())));
                    Espresso.onView(withId(R.id.register_password_input)).check(matches(withText(usersData.getPassword())));
                }
            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
        RootRef.child("Users").child(phone).removeValue();
        try
        {
            Thread.sleep(500);
        }
        catch (InterruptedException e)
        {
            e.printStackTrace();
        }
    }

    @After
    public void tearDown() throws Exception
    {

    }
}