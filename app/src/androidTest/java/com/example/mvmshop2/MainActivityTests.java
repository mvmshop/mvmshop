package com.example.mvmshop2;

import android.app.Activity;
import android.app.Instrumentation;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.platform.app.InstrumentationRegistry;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.junit.Assert.*;

public class MainActivityTests
{

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<MainActivity>(MainActivity.class);
    private MainActivity mActivity = null;
    Instrumentation.ActivityMonitor monitor1 = InstrumentationRegistry.getInstrumentation().addMonitor(LoginActivity.class.getName(), null, false);
    Instrumentation.ActivityMonitor monitor2 = InstrumentationRegistry.getInstrumentation().addMonitor(RegisterActivity.class.getName(), null, false);

    @Before
    public void setUp() throws Exception
    {
        mActivity = mActivityTestRule.getActivity();
    }

    @Test
    public void launchLoginActivityOnButtonClick()
    {
        assertNotNull(mActivity.findViewById(R.id.btnUserLogin));


        Espresso.onView(withId(R.id.btnUserLogin)).perform(click());
        Activity loginActivity = InstrumentationRegistry.getInstrumentation().waitForMonitorWithTimeout(monitor1, 5000);
        assertNotNull(loginActivity);
    }

    @Test
    public void launchRegisterActivityOnButtonClick()
    {
        assertNotNull(mActivity.findViewById(R.id.btnUserSignup));


        Espresso.onView(withId(R.id.btnUserSignup)).perform(click());
        Activity registerActivity = InstrumentationRegistry.getInstrumentation().waitForMonitorWithTimeout(monitor2, 5000);
        assertNotNull(registerActivity);
    }

    @After
    public void tearDown() throws Exception
    {
        mActivity = null;
    }
}