package com.example.mvmshop2;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.ext.junit.runners.AndroidJUnit4;
import androidx.test.rule.ActivityTestRule;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;



import static org.junit.Assert.*;
@RunWith(AndroidJUnit4.class)
public class LoginActivityInputTests
{
    @Rule
    public ActivityTestRule<LoginActivity> mActivityTestRule = new ActivityTestRule<LoginActivity>(LoginActivity.class);


    private String fullName = "arunas Balionas";
    private String emailAddress = "labas@gmail.com";
    private String phone = "1337";
    private String password = "mario";
    private String password2 = "mario1";

    @Before
    public void setUp() throws Exception
    {
    }

    @Test
    public void passwordIsEmpty()
    {
        Espresso.onView(withId(R.id.login_phone_number_input)).perform(typeText(phone), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_button)).perform(click());
        Espresso.onView(withText(R.string.password_is_empty)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

    @Test
    public void successfulLogin()
    {
        Espresso.onView(withId(R.id.login_phone_number_input)).perform(typeText(phone), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_password_input)).perform(typeText(password),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_button)).perform(click());
        Espresso.onView(withText("Prisijungėte sėkmingai")).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }

    @Test
    public void testPhoneNumberIsEmpty()
    {

        Espresso.onView(withId(R.id.login_password_input)).perform(typeText(password2),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_button)).perform(click());
        Espresso.onView(withText(R.string.phone_number_is_empty)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));
    }
    @Test
    public void testWrongPassword ()
    {

        Espresso.onView(withId(R.id.login_phone_number_input)).perform(typeText(phone), ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_password_input)).perform(typeText(password2),ViewActions.closeSoftKeyboard());
        Espresso.onView(withId(R.id.login_button)).perform(click());
        Espresso.onView(withText(R.string.test_login)).inRoot(new ToastMatcher()).check(matches(isDisplayed()));

    }

    @After
    public void tearDown() throws Exception

    {
    }
}