package com.example.mvmshop2.ModelClasses;

public class Users
{
    private String name, phone, password, email, image, address;

    public Users()
    {

    }

    public Users(String name, String phone, String password, String email)
    {
        this.name = name;
        this.phone = phone;
        this.password = password;
        this.email = email;
        this.address=address;
        this.image=image;
    }

    public String getFullName() {
        return name;
    }

    public String getPhone() {
        return phone;
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

    public void setFullName(String fullName) {
        this.name = fullName;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getImage() { return image;  }

    public void setImage(String image) { this.image = image; }

    public String getAddress() { return address; }

    public void setAddress(String address) { this.address = address; }
}
