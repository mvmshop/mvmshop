package com.example.mvmshop2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.mvmshop2.Admin.AdminMaintainProductsActivity;
import com.example.mvmshop2.ModelClasses.Products;
import com.example.mvmshop2.ViewHolder.ProductViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.squareup.picasso.Picasso;

public class CategoryActivity extends AppCompatActivity
{
    private RecyclerView categoryList;
    String name="";
    String name2="";
    FirebaseRecyclerOptions<Products> options;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        TextView textView = findViewById(R.id.category_n);

        Intent intent = getIntent();
        name  = intent.getStringExtra("CategoryName");
        textView.setText(name);


        if(name.equals("SSD"))
            name2 = "ssds";
        else if (name.equals("Klaviatūros"))
            name2 = "keyboards";
        else if(name.equals("Pelės"))
            name2 = "mice";
        else if(name.equals("Kilimėliai"))
            name2 = "mousepads";
        else if(name.equals("Ausinės"))
            name2 = "headphones";
        else if(name.equals("Kameros"))
            name2 = "cameras";


        categoryList = findViewById(R.id.category_list);
        categoryList.setLayoutManager(new LinearLayoutManager(CategoryActivity.this));
    }

    @Override
    protected void onStart()
    {
        super.onStart();



        DatabaseReference reference = FirebaseDatabase.getInstance().getReference().child("Products");

        if(name2.equals(""))
        {
            options = new FirebaseRecyclerOptions.Builder<Products>()
                    .setQuery(reference, Products.class)
                    .build();
        }
        else
        {
            options = new FirebaseRecyclerOptions.Builder<Products>()
                    .setQuery(reference.orderByChild("category").equalTo(name2), Products.class)
                    .build();

        }

        FirebaseRecyclerAdapter<Products, ProductViewHolder> adapter =
                new FirebaseRecyclerAdapter<Products, ProductViewHolder>(options) {
                    @Override
                    protected void onBindViewHolder(@NonNull ProductViewHolder holder, int position, @NonNull final Products model)
                    {
                        holder.txtProductName.setText(model.getPname());
                        holder.txtProductDescription.setText(model.getDescription());
                        holder.txtProductPrice.setText("Kaina = " + model.getPrice() + "€");
                        Picasso.get().load(model.getImage()).into(holder.imageView);


                        holder.itemView.setOnClickListener(new View.OnClickListener()
                        {
                            @Override
                            public void onClick(View view)
                            {
                                    Intent intent = new Intent(CategoryActivity.this, ProductDetailsActivity.class);
                                    intent.putExtra("pid", model.getPid());
                                    startActivity(intent);


                            }
                        });
                    }

                    @NonNull
                    @Override
                    public ProductViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
                    {
                        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.product_items_layout, parent, false);
                        ProductViewHolder holder = new ProductViewHolder(view);
                        return holder;
                    }
                };
        categoryList.setAdapter(adapter);
        adapter.startListening();
    }
}
