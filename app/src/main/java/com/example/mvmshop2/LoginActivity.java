package com.example.mvmshop2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.mvmshop2.Admin.AdminCategoryActivity;
import com.example.mvmshop2.ModelClasses.Users;
import com.example.mvmshop2.Prevelent.Prevelent;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.rey.material.widget.CheckBox;


import io.paperdb.Paper;


public class LoginActivity extends AppCompatActivity
{
    private EditText InputNumber, InputPassword;
    private Button LoginButton;
    private ProgressDialog loadingBar;
    private String parentName = "Users";
    private com.rey.material.widget.CheckBox checkBoxRemember;
    private TextView AdminLink, NotAdminLink,  ForgetPasswordLink;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        LoginButton = (Button) findViewById(R.id.login_button);
        InputNumber = (EditText) findViewById(R.id.login_phone_number_input);
        InputPassword = (EditText) findViewById(R.id.login_password_input);
        loadingBar = new ProgressDialog(this);
        AdminLink = (TextView) findViewById(R.id.admin_panel_link);
        NotAdminLink = (TextView) findViewById(R.id.not_admin_panel_link);
        checkBoxRemember = (CheckBox) findViewById(R.id.remember);
        ForgetPasswordLink = findViewById(R.id.forget_password_link);
        Paper.init(this);

        LoginButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                LoginUser();
            }
        });

        ForgetPasswordLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(LoginActivity.this, ResetPasswordActivity.class);
                intent.putExtra("check", "login");
                startActivity(intent);
            }
        });

        AdminLink.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Admin prisijungimas");
                AdminLink.setVisibility(View.INVISIBLE);
                NotAdminLink.setVisibility(View.VISIBLE);
                parentName = "Admins";
            }
        });

        NotAdminLink.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                LoginButton.setText("Prisijungimas");
                AdminLink.setVisibility(View.VISIBLE);
                NotAdminLink.setVisibility(View.INVISIBLE);
                parentName = "Users";
            }
        });

    }
    private void LoginUser()
    {
        String phone = InputNumber.getText().toString().trim();
        String password = InputPassword.getText().toString();
        if(TextUtils.isEmpty(phone))
            Toast.makeText(this, "Prašome įvesti telefoną", Toast.LENGTH_SHORT).show();
        else if (TextUtils.isEmpty(password))
            Toast.makeText(this, "Prašome įvesti slaptažodį", Toast.LENGTH_SHORT).show();
        else
        {
            loadingBar.setTitle("Prisijungti");
            loadingBar.setMessage("Palaukite kol patvirtinsime");
            loadingBar.setCanceledOnTouchOutside(false);
            loadingBar.show();
            AllowAccessToAccount( phone,  password);

        }
    }
    private void AllowAccessToAccount(final String phone, final String password)
    {
        if(checkBoxRemember.isChecked())
        {
            Paper.book().write(Prevelent.UserPhoneKey, phone);
            Paper.book().write(Prevelent.UserPasswordKey, password);
        }

        final DatabaseReference RootRef;
        RootRef = FirebaseDatabase.getInstance().getReference();

        RootRef.addListenerForSingleValueEvent(new ValueEventListener()
        {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot)
            {
                if (dataSnapshot.child(parentName).child(phone).exists())
                {

                    Users usersData = dataSnapshot.child(parentName).child(phone).getValue(Users.class);
                    if (usersData != null)
                    {
                        if (usersData.getPhone().equals(phone))
                        {
                            if (usersData.getPassword().equals(password))
                            {
                                if (parentName.equals("Admins"))
                                {
                                    Toast.makeText(LoginActivity.this, "Prisijungėte sėkmingai (Admin)", Toast.LENGTH_SHORT).show();
                                    loadingBar.dismiss();
                                    Intent intent = new Intent(LoginActivity.this, AdminCategoryActivity.class);
                                    startActivity(intent);
                                }
                                else if (parentName.equals("Users"))
                                {
                                    Toast.makeText(LoginActivity.this, "Prisijungėte sėkmingai", Toast.LENGTH_SHORT).show();
                                    loadingBar.dismiss();

                                    Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                                    Prevelent.users = usersData;
                                    startActivity(intent);
                                }


                            } else {
                                loadingBar.dismiss();
                                Toast.makeText(LoginActivity.this, "Telefonas arba slaptažodis įvestas neteisingai", Toast.LENGTH_SHORT).show();

                            }

                        }
                    }
                }
                else
                {
                    Toast.makeText(LoginActivity.this, "Paskyra su šiuo " + phone + " neegzistuoja", Toast.LENGTH_SHORT).show();
                    loadingBar.dismiss();
                }

            }

            @Override
            public void onCancelled(@NonNull DatabaseError databaseError)
            {

            }
        });
    }
}
