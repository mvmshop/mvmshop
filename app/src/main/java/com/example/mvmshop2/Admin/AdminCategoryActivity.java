package com.example.mvmshop2.Admin;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.example.mvmshop2.HomeActivity;
import com.example.mvmshop2.MainActivity;
import com.example.mvmshop2.R;

public class AdminCategoryActivity extends AppCompatActivity
{

    private ImageView cameras, keyboards;
    private ImageView  headphones, mouses;
    private ImageView  mousepads, ssds;
    private Button LogoutBtn, CheckOrdersBtn, maintainProductsBtn;
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin_category);



        LogoutBtn = (Button) findViewById(R.id.admin_logout_btn);
        CheckOrdersBtn = (Button) findViewById(R.id.check_orders_btn);
        maintainProductsBtn = (Button) findViewById(R.id.maintain_btn);

        maintainProductsBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, HomeActivity.class);
                intent.putExtra("Admin", "Admin");
                startActivity(intent);
            }
        });


        LogoutBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, MainActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                finish();
            }
        });

        CheckOrdersBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewOrdersActivity.class);
                startActivity(intent);
            }
        });


        cameras= (ImageView) findViewById(R.id.Cameras);
        keyboards= (ImageView) findViewById(R.id.keyboards);
        headphones= (ImageView) findViewById(R.id.headphones);
        mouses= (ImageView) findViewById(R.id.mouses);
        mousepads= (ImageView) findViewById(R.id.mousepads);
        ssds= (ImageView) findViewById(R.id.ssds);


        cameras.setOnClickListener(new View.OnClickListener()
       {
         @Override
          public void onClick(View v)
         {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","cameras");
                startActivity(intent);

         }
       });


        keyboards.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","keyboards");
                startActivity(intent);
            }
        });

        headphones.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","headphones");
                startActivity(intent);
            }
        });

        mouses.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","mice");
                startActivity(intent);
            }
        });

        mousepads.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","mousepads");
                startActivity(intent);
            }
        });

        ssds.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                Intent intent = new Intent(AdminCategoryActivity.this, AdminNewProductActivity.class);
                intent.putExtra("Category","ssds");
                startActivity(intent);
            }
        });
    }
}
