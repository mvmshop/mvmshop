package com.example.mvmshop2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.mvmshop2.ModelClasses.Cart;
import com.example.mvmshop2.Prevelent.Prevelent;
import com.example.mvmshop2.ViewHolder.CartViewHolder;
import com.firebase.ui.database.FirebaseRecyclerAdapter;
import com.firebase.ui.database.FirebaseRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

public class CartActivity extends AppCompatActivity
{
    private RecyclerView recyclerView;
    private RecyclerView.LayoutManager layoutManager;

    private Button NextProcessBtn;
    private TextView txtTotalAmount, txtMsg1;

    private double overTotalPrice = 0;



    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cart);


        recyclerView = findViewById(R.id.cart_list);
        recyclerView.setHasFixedSize(true);
        layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        NextProcessBtn = (Button) findViewById(R.id.next_btn);
        txtTotalAmount = (TextView) findViewById(R.id.total_price);
        txtMsg1 = (TextView) findViewById(R.id.msg1);


        NextProcessBtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {


                Intent intent = new Intent(CartActivity.this, ConfirmFinalOrderActivity.class);
                intent.putExtra("Total Price", String.valueOf(overTotalPrice));
                startActivity(intent);
                finish();
            }
        });
    }


    @Override
    protected void onStart()
    {
        super.onStart();

        CheckOrderState();


        final DatabaseReference cartListRef = FirebaseDatabase.getInstance().getReference().child("Cart List");

        if(Prevelent.users != null)
        {
            FirebaseRecyclerOptions<Cart> options =
                    new FirebaseRecyclerOptions.Builder<Cart>()
                            .setQuery(cartListRef.child("User View")
                                    .child(Prevelent.users.getPhone())
                                    .child("Products"), Cart.class)
                            .build();

            FirebaseRecyclerAdapter<Cart, CartViewHolder> adapter
                    = new FirebaseRecyclerAdapter<Cart, CartViewHolder>(options)
            {
                @Override
                protected void onBindViewHolder(@NonNull CartViewHolder holder, int position, @NonNull final Cart model)
                {
                    holder.txtProductQuantity.setText("Kiekis = " + model.getQuantity());
                    holder.txtProductPrice.setText("Kaina " + model.getPrice() + "€");
                    holder.txtProductName.setText(model.getPname());

                    double oneTyprProductTPrice = ((Double.valueOf(model.getPrice()))) * Integer.valueOf(model.getQuantity());
                    overTotalPrice = overTotalPrice + oneTyprProductTPrice;
                    txtTotalAmount.setText("Galutinė suma = " + String.valueOf(overTotalPrice) + '€');

                    holder.itemView.setOnClickListener(new View.OnClickListener()
                    {
                        @Override
                        public void onClick(View view)
                        {
                            CharSequence options[] = new CharSequence[]
                                    {
                                            "Redaguoti",
                                            "Pašalinti"
                                    };
                            AlertDialog.Builder builder = new AlertDialog.Builder(CartActivity.this);
                            builder.setTitle("Krepšelio nustatymai:");

                            builder.setItems(options, new DialogInterface.OnClickListener()
                            {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i)
                                {
                                    if (i == 0)
                                    {
                                        Intent intent = new Intent(CartActivity.this, ProductDetailsActivity.class);
                                        intent.putExtra("pid", model.getPid());
                                        startActivity(intent);
                                    }
                                    if (i == 1)
                                    {
                                        cartListRef.child("User View")
                                                .child(Prevelent.users.getPhone())
                                                .child("Products")
                                                .child(model.getPid())
                                                .removeValue()
                                                .addOnCompleteListener(new OnCompleteListener<Void>()
                                                {
                                                    @Override
                                                    public void onComplete(@NonNull Task<Void> task)
                                                    {
                                                        if (task.isSuccessful())
                                                        {
                                                            Toast.makeText(CartActivity.this, "Produktas pašalintas sėkmingai.", Toast.LENGTH_SHORT).show();

                                                            Intent intent = new Intent(CartActivity.this, HomeActivity.class);
                                                            startActivity(intent);
                                                        }
                                                    }
                                                });
                                    }
                                }
                            });
                            builder.show();
                        }
                    });

                }


                @NonNull
                @Override
                public CartViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
                {
                    View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.cart_items_layout, parent, false);
                    CartViewHolder holder = new CartViewHolder(view);
                    return holder;
                }
            };

            recyclerView.setAdapter(adapter);
            adapter.startListening();
        }

    }



    private void CheckOrderState()
    {

        DatabaseReference ordersRef;
        if(Prevelent.users != null)
        {
            ordersRef = FirebaseDatabase.getInstance().getReference().child("Orders").child(Prevelent.users.getPhone());

            ordersRef.addValueEventListener(new ValueEventListener()
            {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot)
                {
                    if (dataSnapshot.exists()) {
                        String shippingState = dataSnapshot.child("state").getValue().toString();
                        String userName = dataSnapshot.child("name").getValue().toString();

                        if (shippingState.equals("shipped")) {
                            txtTotalAmount.setText("Gerb. " + userName + "\n užsakymas išsiūstas sėkmingai.");
                            recyclerView.setVisibility(View.GONE);

                            txtMsg1.setVisibility(View.VISIBLE);
                            txtMsg1.setText("Sveikiname, jūsų galutinis užsakymas buvo sėkmingai išsiūstas. Netrukus jūsų užsakymas bus prie jūsų namų durų!");
                            NextProcessBtn.setVisibility(View.GONE);

                            Toast.makeText(CartActivity.this, "Jūs galėsite pirkti daugiau produktų, kuomet gausite patvirtinimą, kad pirmasis užsakymas buvo sėkmingas.", Toast.LENGTH_SHORT).show();
                        }
                        else if (shippingState.equals("not shipped"))
                        {
                            txtTotalAmount.setText("Pristatymo būsena = Nepristatytas");
                            recyclerView.setVisibility(View.GONE);

                            txtMsg1.setVisibility(View.VISIBLE);
                            NextProcessBtn.setVisibility(View.GONE);

                            Toast.makeText(CartActivity.this, "Jūs galėsite pirkti daugiau produktų, kuomet gausite patvirtinimą, kad pirmasis užsakymas buvo sėkmingas.", Toast.LENGTH_SHORT).show();
                        }
                    }
                }

                @Override
                public void onCancelled(DatabaseError databaseError)
                {

                }
            });
        }
    }
}